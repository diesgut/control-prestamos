import {EstadoPrestamoParametroCons, TipoOperacionParametroCons} from './constants/parametrosCons';
import {Contacto} from './contacto';

export class Prestamo {

  public id: number;
  public idContacto: number;
  public monto: number;
  public tipoOperacion: number;
  public moneda: number;
  public comentario: string;
  public fechaRegistro: string;
  public fechaPrestamo: string;
  public fechaPago: string;
  public fechaVencimiento: string;
  public repetir: number;
  public intervaloRepeticion: number;
  public estado: number;
  public tipoRepeticion: number;

  public contacto: Contacto;





  constructor() {

    this.intervaloRepeticion = 0;
    this.tipoRepeticion = 0;
    this.repetir = 0;

    this.contacto = new Contacto(null, null, null);
    /*
        this.idContacto = null;
        this.cantidad = null;
        this.tipoPrestamo = null;
        this.moneda = "";
        this.comentario = "";
        this.fechaRegistro = null;
        this.fechaPrestamo = null;
        this.fechaVencimiento = null;
        this.repetir = 0;
        this.intervaloRepeticion = 0;
        this.tipoRepeticion = "";*/
  }

  create(id: number,
    idContacto: number,
    monto: number,
    tipoOperacion: number,
    moneda: number,
    comentario: string,
    fechaRegistro: string,
    fechaPrestamo: string,
    fechaVencimiento: string,
    fechaPago: string,
    repetir: number,
    intervaloRepeticion: number,
    tipoRepeticion: number,
    estado: number) {
    this.id = id;
    this.idContacto = idContacto;
    this.monto = monto;
    this.tipoOperacion = tipoOperacion;
    this.moneda = moneda;
    this.comentario = comentario;
    this.fechaRegistro = fechaRegistro;
    this.fechaPrestamo = fechaPrestamo;
    this.fechaVencimiento = fechaVencimiento;
    this.fechaPago=fechaPago;
    this.repetir = repetir;
    this.intervaloRepeticion = intervaloRepeticion;
    this.tipoRepeticion = tipoRepeticion;
    this.estado = estado;
  }

  isEstadoPendiente(): boolean {
    if (this.estado == EstadoPrestamoParametroCons.PENDIENTE.id) {
      return true;
    }
    return false;
  }

  isEstadoVencido(): boolean {
    if (this.estado == EstadoPrestamoParametroCons.VENCIDO.id) {
      return true;
    }
    return false;
  }

  isEstadoPagado(): boolean {
    if (this.estado == EstadoPrestamoParametroCons.PAGADO.id) {
      return true;
    }
    return false;
  }

  color(): string {
    let color: string = "default";
    if (this.isEstadoPendiente()) {
      color= "primary";
    } else if (this.isEstadoVencido()) {
      color = "danger";
    } else if (this.isEstadoPagado()) {
      color = "secondary";
    }
    return color;
  }

  icon():string{
    let icon: string = "ion-ionic";
    if (this.isEstadoPendiente()) {
      icon= "ios-alarm";
    } else if (this.isEstadoVencido()) {
      icon = "ios-alarm";
    } else if (this.isEstadoPagado()) {
      icon = "md-checkmark-circle";
    }
    return icon;
  }

  isTipoOperacionPresto(): boolean {
    if (this.tipoOperacion == TipoOperacionParametroCons.PRESTO.id) {
      return true;
    }
    return false;
  }

  isTipoOperacionPresta(): boolean {
    if (this.tipoOperacion == TipoOperacionParametroCons.PRESTA.id) {
      return true;
    }
    return false;
  }


}
