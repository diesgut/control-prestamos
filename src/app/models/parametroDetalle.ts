export class ParametroDetalle {
  constructor(
    public id: number,
    public idParametro: number,
    public nombre: string,
    public descripcion: string,
    public simbolo1: string,
    public codigo1: string,
    public id1: number) {

  }

}
