export const MonedaParametrosCons = {
  PEN: { id: 3 },
  USD: { id: 4 },
  PESO: { id: 8 },
  EUR: { id: 9 }
}

export const TipoOperacionParametroCons = {
  PRESTO: { id: 1 },
  PRESTA: { id: 2 },
}

export const EstadoPrestamoParametroCons = {
  PENDIENTE: { id: 5 },
  VENCIDO: { id: 6 },
  PAGADO: { id: 7 }
}

export const ConfiguracionParametroCons = {
  MONEDA: { id: 10 }
}
