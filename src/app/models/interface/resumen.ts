export interface Resumen {
  estado: number;
  cantidad: number;
  monto: number;
}
