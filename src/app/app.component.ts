import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {  SQLiteObject } from '@ionic-native/sqlite';
import { PrestamosComponent } from '../pages/prestamos/prestamos.component';
//import { PrestamoNuevoComponent } from '../pages/prestamo-nuevo/prestamo-nuevo.component';
import { HomeComponent } from '../pages/home/home.component';
import { MonedaComponent } from '../pages/moneda/moneda.component';
import {PrestamoService} from './services/prestamo.service';
import {TipoOperacionParametroCons} from './models/constants/parametrosCons';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = null;

  homeComponent: any = HomeComponent;
  monedaComponent: any = MonedaComponent;

  pages: Array<{ title: string, component: any, params: any }>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, private prestamoService: PrestamoService) {
    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Prestamos', component: PrestamosComponent, params: { tipoOperacion: TipoOperacionParametroCons.PRESTO.id } },
      { title: 'Deudas', component: PrestamosComponent, params: { tipoOperacion: TipoOperacionParametroCons.PRESTA.id } }
    ];

    this.initializeApp();

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

      console.log('inicio de la aplicacion');


      if (this.platform.is('mobileweb')) {
        console.log('Plataforma mobileweb');
        //    this.rootPage = PrestamosComponent;
        //  setTimeout(() => console.log('esperamos'), 2000);
        this.statusBar.styleDefault();
        this.splashScreen.hide();
        this.openPage(HomeComponent);
      } else if (this.platform.is('android')) {
        console.log('Plataforma android');

        this.prestamoService.openDatabase().then((db: SQLiteObject) => {
          this.prestamoService.db = db;
          this.prestamoService.createTables().then(success => {
            console.log('app.component crear tablas ');
            console.dir(success);
            this.prestamoService.defaultData().then(tasks => {
              //  this.rootPage = PrestamosComponent;
              this.statusBar.styleDefault();

              setTimeout(() => {
                this.splashScreen.hide();
              }, 100);
              this.openPage(HomeComponent);
            });
          });
        });
      }


    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario

    if (page.component == null) {
      this.nav.setRoot(page);
    } else {
      this.nav.setRoot(page.component, { tipoOperacion: page.params.tipoOperacion });
    }
  }
}
