import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import {Contacto} from '../models/contacto';
import {Prestamo} from '../models/prestamo';
import {Resumen} from '../models/interface/resumen';
import {ParametroDetalle} from '../models/parametroDetalle';
import {Parametro} from '../models/parametro';
import {ParametroEnum} from '../models/enums/parametroEnum';
import {ConfiguracionParametroCons, TipoOperacionParametroCons, MonedaParametrosCons, EstadoPrestamoParametroCons} from '../models/constants/parametrosCons';

@Injectable()
export class PrestamoService {
  /*
    private contactos: Contacto[] = [];
    private prestamos: Prestamo[] = [];*/
  parametroDetalles: ParametroDetalle[] = [];

  db: SQLiteObject = null;


  constructor(private sqlite: SQLite) {
    console.log('Prestamo Service iniciado');
    //  this.db = new SQLite();
    //  this.openDatabase();


    this.parametroDetalles.push(new ParametroDetalle(TipoOperacionParametroCons.PRESTO.id, ParametroEnum.TIPO_OPERACION, "Presto", "Yo Presto", "", "", 0));
    this.parametroDetalles.push(new ParametroDetalle(TipoOperacionParametroCons.PRESTA.id, ParametroEnum.TIPO_OPERACION, "Presta", "Me prestan", "", "", 0));

    this.parametroDetalles.push(new ParametroDetalle(MonedaParametrosCons.PEN.id, ParametroEnum.MONEDA, "Soles", "Yo Presto", "S", "PEN", 0));
    this.parametroDetalles.push(new ParametroDetalle(MonedaParametrosCons.USD.id, ParametroEnum.MONEDA, "Dolares", "Me prestan", "$", "USD", 0));

    this.parametroDetalles.push(new ParametroDetalle(EstadoPrestamoParametroCons.PENDIENTE.id, ParametroEnum.ESTADO_PRESTAMO, "Pendiente", "", "", "", 0));
    this.parametroDetalles.push(new ParametroDetalle(EstadoPrestamoParametroCons.VENCIDO.id, ParametroEnum.ESTADO_PRESTAMO, "Vencido", "", "", "", 0));
    this.parametroDetalles.push(new ParametroDetalle(EstadoPrestamoParametroCons.PAGADO.id, ParametroEnum.ESTADO_PRESTAMO, "Pagado", "", "", "", 0));

  }

  openDatabase() {
    /*
    return this.db.openDatabase({
      name: 'prestamoz2.db',
      location: 'default' // the location field is required
    });*/
    return this.sqlite.create({
      name: 'prestamos_diesgut.db',
      location: 'default'
    });
  }

  createTables() {
    let sql = '';
    let count = 0;
    return new Promise((resolve, reject) => {
      console.log('Crear tablas');

      sql = 'SELECT count(*) count FROM sqlite_master WHERE type="table"';
      let promises = [];
      this.db.executeSql(sql, []).then(response => {
        if (response != null && response != undefined && response.rows.length > 0) {
          for (let index = 0; index < response.rows.length; index++) {
            console.log('tablas');
            console.dir(response.rows.item(index));
            count = response.rows.item(index).count;
          }
        }
        if (count == 0) {


          promises.push();
          this.db.transaction(function(tx) {

            sql = 'CREATE TABLE IF NOT EXISTS prestamos(id INTEGER PRIMARY KEY AUTOINCREMENT, idContacto INTEGER, monto REAL, tipoOperacion INTEGER,'
              + ' moneda INTEGER, comentario TEXT, fechaRegistro TEXT, fechaPrestamo TEXT, fechaVencimiento TEXT, fechaPago TEXT, repetir INTEGER, '
              + ' intervaloRepeticion INTEGER, estado INTEGER, tipoRepeticion INTEGER)';
            promises.push(tx.executeSql(sql, [], function(tx, results) {
              console.log('Tabla prestamos creada ' + JSON.stringify(results));
            }, function(error) {
              console.log('Error al crear tabla prestamos ' + error);
            }));

            sql = 'CREATE TABLE IF NOT EXISTS contactos(id INTEGER PRIMARY KEY AUTOINCREMENT, nombre TEXT, telefonoMovil TEXT)';
            promises.push(tx.executeSql(sql, [], function(tx, results) {
              console.log('Tabla contactos creada ' + JSON.stringify(results));
            },
              function(error) {
                console.log('Error al crear tabla contactos ' + error);
              }
            ));

            sql = 'CREATE TABLE IF NOT EXISTS parametros(id INTEGER PRIMARY KEY AUTOINCREMENT, nombre TEXT, descripcion TEXT)';
            promises.push(
              tx.executeSql(sql, [],
                function(tx, results) {
                  console.log('Tabla parametros creada ' + JSON.stringify(results));
                },
                function(error) {
                  console.log('Error al crear tabla contactos ' + error);
                }
              ));

            sql = 'CREATE TABLE IF NOT EXISTS parametro_detalles(id INTEGER PRIMARY KEY AUTOINCREMENT, idParametro INTEGER, nombre TEXT, descripcion TEXT, simbolo1 TEXT, codigo1 TEXT, id1 INTEGER)';
            promises.push(
              tx.executeSql(sql, [],
                function(tx, results) {
                  console.log('Tabla parametro_detalles creada ' + JSON.stringify(results));
                },
                function(error) {
                  console.log('Error al crear tabla contactos ' + error);
                }
              ));

          });

          return Promise.all(promises).then(function(success) {
            resolve(success);
          }, function(err) {
            reject(err);
          });;
        } else {
          console.log('las tablas ya estaban creadas');
          resolve(true);
        }

      },
        function(error) {

          console.log('Error al contar las tablas ' + error);
          return Promise.reject(error);
        }
      );
      console.log('termino crear tablas');

    });
  }

  defaultData() {
    let sqlCount = 'SELECT count(*) cantidad from parametros ';
    let cantidad = 0;
    this.db.executeSql(sqlCount, []).then(response => {
      if (response != null && response != undefined && response.rows.length > 0) {
        for (let index = 0; index < response.rows.length; index++) {
          cantidad = response.rows.item(index).cantidad;
        }
      }
      if (cantidad == 0) {


        console.log('insertara los parametros por defecto');
        let parametro = new Parametro(ParametroEnum.MONEDA, 'Monedas', 'Monedas de la aplicación');
        let parametroDetalles: ParametroDetalle[] = [];

        let parametroDetalle = new ParametroDetalle(MonedaParametrosCons.PEN.id, ParametroEnum.MONEDA, 'Nuevo Sol', 'Nuevo Sol', 's/', 'PEN', null);
        parametroDetalles.push(parametroDetalle);

        parametroDetalle = new ParametroDetalle(MonedaParametrosCons.USD.id, ParametroEnum.MONEDA, 'Dolar Americano', 'NDolar Americano', '$/', 'USD', null);
        parametroDetalles.push(parametroDetalle);

        parametroDetalle = new ParametroDetalle(MonedaParametrosCons.PESO.id, ParametroEnum.MONEDA, 'Peso', 'Peso', '$/', 'PES', null);
        parametroDetalles.push(parametroDetalle);

        parametroDetalle = new ParametroDetalle(MonedaParametrosCons.EUR.id, ParametroEnum.MONEDA, 'Euro', 'Euro', '€', 'EUR', null);
        parametroDetalles.push(parametroDetalle);

        this.insertParametroDefault(parametro, parametroDetalles);

        parametroDetalles = [];
        parametro = new Parametro(ParametroEnum.CONFIGURACION, 'CONFIGURACION', 'CONFIGURACION');
        parametroDetalle = new ParametroDetalle(ConfiguracionParametroCons.MONEDA.id, ParametroEnum.CONFIGURACION, '', '', '', '', MonedaParametrosCons.PEN.id);
        parametroDetalles.push(parametroDetalle);

        this.insertParametroDefault(parametro, parametroDetalles);
      }

    }, error => {
      console.log('Error al consultar la data por defecto');
      console.dir(error);
    });
    return Promise.resolve();
  }

  insertParametroDefault(parametro: Parametro, parametroDetalles: ParametroDetalle[]) {
    let argParametro: any[] = [];
    let argParametroDetalle: any[] = [];

    argParametro.push(parametro.id);
    argParametro.push(parametro.nombre);
    argParametro.push(parametro.descripcion);

    this.db.transaction(function(tx) {
      tx.executeSql("INSERT INTO parametros(id,nombre, descripcion) VALUES(?,?,?)",
        argParametro,
        function(tx, results) {
          console.log('Parametro guardado ' + parametro.nombre);
          console.dir(results);

          for (let parametroDetalle of parametroDetalles) {

            argParametroDetalle = [];
            argParametroDetalle.push(parametroDetalle.id);
            argParametroDetalle.push(parametroDetalle.idParametro);
            argParametroDetalle.push(parametroDetalle.nombre);
            argParametroDetalle.push(parametroDetalle.descripcion);
            argParametroDetalle.push(parametroDetalle.simbolo1);
            argParametroDetalle.push(parametroDetalle.codigo1);
            argParametroDetalle.push(parametroDetalle.id1);

            tx.executeSql("INSERT INTO parametro_detalles(id, idParametro, nombre, descripcion,simbolo1, codigo1,id1) values(?,?,?,?,?,?,?)",
              argParametroDetalle,
              function(tx, results) {
                console.log('Parametro detalle guardado ' + parametroDetalle.nombre);
                console.dir(results);
              },
              function(error) {
                console.log('Error al grabar el parametro_detalles ' + parametroDetalle.nombre);
                console.dir(error);
              });
          }
        },
        function(error) {
          console.log('Error al grabar el parametro ' + parametro.nombre);
          console.dir(error);
        });

    }); /*
    .catch((err) => {
      console.error('Error al insertar los parametros por default', err);
    });
    */
  }

  guardarPrestamoBD(prestamo: Prestamo, contacto: Contacto) {
    let lastInsertContactId;
    let lastPrestamoId;
    this.db.transaction(function(tx) {

      tx.executeSql("INSERT INTO contactos(nombre, telefonoMovil) VALUES(?,?)",
        [contacto.nombre, contacto.telefonoMovil],
        function(tx, results) {
          console.log('Contacto guardado ');
          console.dir(results);
          lastInsertContactId = results.insertId;

          tx.executeSql("INSERT INTO prestamos(idContacto, monto,tipoOperacion,moneda,comentario,fechaRegistro,fechaPrestamo,fechaVencimiento,repetir,intervaloRepeticion,"
            + " estado,tipoRepeticion) "
            + " VALUES(?,?,?,?,?,date(?),date(?),date(?),?,?,?,?)",
            [lastInsertContactId, prestamo.monto, prestamo.tipoOperacion, prestamo.moneda,
              prestamo.comentario, prestamo.fechaRegistro, prestamo.fechaPrestamo, prestamo.fechaVencimiento,
              prestamo.repetir, prestamo.intervaloRepeticion, prestamo.estado, prestamo.tipoRepeticion],
            function(tx, results) {
              lastPrestamoId = results.insertId;
              console.log('Prestamo guardado ');
              console.dir(results);
            },
            function(error) {
              console.log('Error al grabar el prestamos ');
              console.dir(error);
            });


        },
        function(error) {
          console.log('Error al grabar el cotacto ');
          console.dir(error);
        });



    });
    return Promise.resolve('Prestamo y Contacto guardados con ids ' + lastPrestamoId + ' y ' + lastInsertContactId);
  }

  updatePrestamoBD(prestamo: Prestamo, contacto: Contacto) {

    let today = new Date();
    today.setHours(0, 0, 0, 0);

    this.db.transaction(function(tx) {

      tx.executeSql("update contactos set nombre=?, telefonoMovil=? where id=?",
        [contacto.nombre, contacto.telefonoMovil, contacto.id],
        function(tx, results) {
          console.log('Contacto actualizado ');
          console.dir(results);
        },
        function(error) {
          console.log('Error al actualizar el cotacto ');
          console.dir(error);
        });


      let expiredDateArg = prestamo.fechaVencimiento.split("-");
      let expiredDate = new Date(parseInt(expiredDateArg[0]), (parseInt(expiredDateArg[1]) - 1), parseInt(expiredDateArg[2]), 0, 0, 0, 0);

      if (expiredDate.getTime() >= today.getTime()) {
        prestamo.estado = EstadoPrestamoParametroCons.PENDIENTE.id;
      } else {
        prestamo.estado = EstadoPrestamoParametroCons.VENCIDO.id;
      }

      tx.executeSql("update prestamos set monto=?, moneda=?,comentario=?, estado=?, fechaPrestamo=?, fechaVencimiento=? where id=?",
        [prestamo.monto, prestamo.moneda, prestamo.comentario, prestamo.estado, prestamo.fechaPrestamo, prestamo.fechaVencimiento, prestamo.id],
        function(tx, results) {
          console.log('Prestamo actualizado ');
          console.dir(results);
        },
        function(error) {
          console.log('Error al actualizar el prestamos ');
          console.dir(error);
        });


    });
    return Promise.resolve('Prestamo y Contacto actualizados ');
  }
  // parametro_detalles(id, idParametro, nombre, descripcion,simbolo1, codigo1,id1)
  updateParameter(parametroDetalle: ParametroDetalle) {
    let argParams: any[] = [];
    argParams.push(parametroDetalle.idParametro);
    argParams.push(parametroDetalle.nombre);
    argParams.push(parametroDetalle.descripcion);
    argParams.push(parametroDetalle.simbolo1);
    argParams.push(parametroDetalle.codigo1);
    argParams.push(parametroDetalle.id1);
    argParams.push(parametroDetalle.id);

    this.db.transaction(function(tx) {
      tx.executeSql("update parametro_detalles set idParametro=?, nombre=?,descripcion=?, simbolo1=?, codigo1=?, id1=? where id=?",
        argParams,
        function(tx, results) {
          console.log('parametro_detalles actualizado ');
          console.dir(results);
        },
        function(error) {
          console.log('Error al actualizar el parametro_detalles ');
          console.dir(error);
        });
    });
    return Promise.resolve('parametro_detalles actualizado ');
  }



  getPrestamosBDbyFilter(estado: number) {
    let filtros: any[] = [];
    let sql = 'SELECT pres.id prestamoId, pres.monto, pres.tipoOperacion, pres.moneda, pres.comentario, '
      + ' pres.fechaRegistro, pres.fechaPrestamo, pres.fechaVencimiento, pres.estado, '
      + ' con.id contactoId, con.nombre, con.telefonoMovil '
      + ' FROM prestamos pres '
      + ' inner join contactos con on pres.idContacto=con.id '
      + ' where 1=1 ';
    if (estado != null) {
      sql += ' and pres.tipoOperacion=? ';
      filtros.push(estado);
    }
    sql += ' order by pres.fechaVencimiento desc ';
    return this.db.executeSql(sql, filtros).then(response => {
      let prestamos: Prestamo[] = [];
      if (response != null && response != undefined && response.rows.length > 0) {
        for (let index = 0; index < response.rows.length; index++) {
          let contacto = new Contacto(response.rows.item(index).contactoId, response.rows.item(index).nombre, response.rows.item(index).telefonoMovil);

          let prestamo = new Prestamo();
          prestamo.create(response.rows.item(index).prestamoId, response.rows.item(index).contactoId, response.rows.item(index).monto, response.rows.item(index).tipoOperacion,
            response.rows.item(index).moneda, response.rows.item(index).comentario, response.rows.item(index).fechaRegistro, response.rows.item(index).fechaPrestamo,
            response.rows.item(index).fechaVencimiento, null, 0, 0, 0, response.rows.item(index).estado);
          prestamo.contacto = contacto;
          prestamo.idContacto = contacto.id;



          prestamos.push(prestamo);
        }
      }
      console.log('Cantidad de prestamos encontrados ' + prestamos.length);
      return Promise.resolve(prestamos);
    }, error => {
      return Promise.reject(error);
    });
  }

  getParametrosDetBDbyFilter(parametro: number, id: number) {
    let filtros: any[] = [];
    let sql = 'Select id,idParametro,nombre,descripcion,simbolo1,codigo1,id1 from parametro_detalles';
    sql += ' where 1=1 ';
    if (parametro != null) {
      sql += ' and idParametro=? ';
      filtros.push(parametro);
    }
    if (id != null) {
      sql += ' and id=? ';
      filtros.push(id);
    }
    sql += ' order by nombre ';


    return this.db.executeSql(sql, filtros).then(response => {
      let parametroDetalles: ParametroDetalle[] = [];
      if (response != null && response != undefined && response.rows.length > 0) {
        for (let index = 0; index < response.rows.length; index++) {
          let parametroDetalle = new ParametroDetalle(response.rows.item(index).id, response.rows.item(index).idParametro,
            response.rows.item(index).nombre, response.rows.item(index).descripcion, response.rows.item(index).simbolo1,
            response.rows.item(index).codigo1,
            response.rows.item(index).id1
          );
          parametroDetalles.push(parametroDetalle);
        }
      }
      console.log('Cantidad de parametro detalles encontrados ' + parametroDetalles.length);
      return Promise.resolve(parametroDetalles);
    }, error => {
      return Promise.reject(error);
    });
  }

  getPrestamosResumenBD(tipoOperacion: number) {
    let filtros: any[] = [];
    let sql = 'SELECT estado ,count(*) cantidad, sum(pres.monto) monto '
      + ' FROM prestamos pres '
      + ' where pres.tipoOperacion=? '
      + ' group by pres.estado ';
    filtros.push(tipoOperacion);
    return this.db.executeSql(sql, filtros).then(response => {
      let resumenList: Resumen[] = [];
      if (response != null && response != undefined && response.rows.length > 0) {
        for (let index = 0; index < response.rows.length; index++) {
          let resumen: Resumen = { estado: response.rows.item(index).estado, cantidad: response.rows.item(index).cantidad, monto: response.rows.item(index).monto };
          resumenList.push(resumen);
        }
      }
      console.log(`Tipo ${tipoOperacion}, cantidad ${resumenList.length}`);
      return Promise.resolve(resumenList);
    }, error => {
      return Promise.reject(error);
    });
  }

  pagarPrestadmoBD(prestamoId: number, estadoId: number) {
    let sql = 'update  prestamos set estado=?, fechaPago=date(?) where id=? ';
    return this.db.executeSql(sql, [estadoId, this.convertToDateStr(new Date()), prestamoId]);
  }

  convertToDateStr(date: Date): string {
    let day = date.getDate().toString();
    if (day.length == 1) {
      day = "0" + day;
    }

    let month = date.getMonth().toString();
    if (month.length == 1) {
      month = "0" + month;
    }
    let todayStr = date.getFullYear() + '-' + month + '-' + day;
    return todayStr;
  }

  deletePrestadmoBD(prestamoId: number) {
    let sql = 'delete from prestamos  where id=? ';
    return this.db.executeSql(sql, [prestamoId]);
  }

  updatePrestadmoLS(prestamoId: number, estadoId: number) {
    let prestamosStorage: Prestamo[] = [];
    let today = new Date().toISOString();

    if (localStorage.getItem("prestamos") != null && localStorage.getItem("prestamos") != undefined) {
      prestamosStorage = JSON.parse(localStorage.getItem("prestamos"));
    }
    console.dir(prestamosStorage);
    for (let prestamo of prestamosStorage) {
      console.log('busca ' + prestamo.id);
      if (prestamo.id == prestamoId) {
        console.log('encontro el prestamo');
        prestamo.estado = estadoId;
        prestamo.fechaPago = today;
      }
    }
    localStorage.setItem("prestamos", JSON.stringify(prestamosStorage));
  }

  deletePrestadmoLS(prestamoId: number) {

  }

  guardarPrestamoLS(prestamo: Prestamo, contacto: Contacto) {


    let prestamosStorage: Prestamo[] = [];

    if (localStorage.getItem("prestamos") != null && localStorage.getItem("prestamos") != undefined) {
      prestamosStorage = JSON.parse(localStorage.getItem("prestamos"));
    }
    prestamo.id = prestamosStorage.length;
    prestamo.contacto = contacto;
    prestamosStorage.push(prestamo);
    localStorage.setItem("prestamos", JSON.stringify(prestamosStorage));

  }

  getPrestamosLS(): Prestamo[] {
    if (localStorage.getItem("prestamos") == null) {
      return [];
    }
    let prestamosStorage = JSON.parse(localStorage.getItem("prestamos"));
    let prestamosResult: Prestamo[] = [];

    for (let prest of prestamosStorage) {
      let contacto: Contacto = new Contacto(null, prest.contacto.nombre, prest.contacto.telefonoMovil);
      let prestamo = new Prestamo();
      prestamo.create(1, 0, prest.monto, prest.tipoOperacion, prest.moneda, prest.comentario,
        prest.fechaRegistro, prest.fechaPrestamo, prest.fechaVencimiento, prest.fechaPago, 1, 1, 1, prest.estado);
      prestamo.contacto = contacto;
      prestamosResult.push(prestamo);
    }

    return prestamosResult;
  }

  getParametroDetalleByParent(idParametro: number) {
    let parametroDetallesResult: ParametroDetalle[] = [];
    for (let parametro of this.parametroDetalles) {
      if (parametro.idParametro == idParametro) {
        parametroDetallesResult.push(parametro);
      }
    }
    return parametroDetallesResult;
  }


}
