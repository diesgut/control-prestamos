import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { FormsModule } from '@angular/forms';
import {LOCALE_ID} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { SQLite } from '@ionic-native/sqlite';

import {PrestamoService} from './services/prestamo.service';

import { PrestamosComponent } from '../pages/prestamos/prestamos.component';
import { PrestamoNuevoComponent } from '../pages/prestamo-nuevo/prestamo-nuevo.component';
import { HomeComponent } from '../pages/home/home.component';
import { MonedaComponent } from '../pages/moneda/moneda.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    PrestamosComponent,
    PrestamoNuevoComponent,
    HomeComponent,
    MonedaComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    PrestamosComponent,
    PrestamoNuevoComponent,
    HomeComponent,
    MonedaComponent
  ],
  providers: [
    PrestamoService,
    StatusBar,
    SplashScreen,
    SQLite,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    { provide: LOCALE_ID, useValue: "es" }
  ]
})
export class AppModule {


}
