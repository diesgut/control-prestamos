import { Component, OnInit } from '@angular/core';
import {PrestamoService} from '../../app/services/prestamo.service';
import {NavController, NavParams, ToastController, ActionSheetController, AlertController} from 'ionic-angular';
import { Platform } from 'ionic-angular';
import {PrestamoNuevoComponent} from '../prestamo-nuevo/prestamo-nuevo.component';
import {EstadoPrestamoParametroCons, TipoOperacionParametroCons, ConfiguracionParametroCons} from '../../app/models/constants/parametrosCons';
import {ParametroEnum} from '../../app/models/enums/parametroEnum';

//import {Prestamo} from '../../app/models/prestamo';
import {ParametroDetalle} from '../../app/models/parametroDetalle';
//import { Contacts } from 'ionic-native';

@Component({
  selector: 'app-prestamos',
  templateUrl: 'prestamos.component.html',
})
export class PrestamosComponent implements OnInit {

  prestamos: any[];
  tipoOperacion: number;
  monedaDefault: ParametroDetalle;

  constructor(private _prestamoService: PrestamoService,
    private _navController: NavController,
    private _navParams: NavParams,
    private platform: Platform,
    private toastCtrl: ToastController,
    private actionsheetCtrl: ActionSheetController,
    private alertCtrl: AlertController) {
    this.tipoOperacion = _navParams.get("tipoOperacion");
    this.monedaDefault = new ParametroDetalle(1, 1, "", "", "", "", 1);
  }

  ngOnInit() {

    this.prestamos = [];
    this.cargarPrestamos();



  }

  cargarPrestamos() {
    if (this.platform.is('mobileweb')) {
      console.log('PrestamosComponent mobileweb');
      this.prestamos = this._prestamoService.getPrestamosLS();

    } else if (this.platform.is('android')) {
      console.log('PrestamosComponent android');
      this._prestamoService.getPrestamosBDbyFilter(this.tipoOperacion).then((prestamoss) => {
        this.prestamos = prestamoss;
        console.log('cantidad prestamos ' + this.prestamos.length);
      }, (error) => {
        console.log('Error al consultar los prestamos guardados');
        console.dir(error);
      });


      this._prestamoService.getParametrosDetBDbyFilter(ParametroEnum.CONFIGURACION, ConfiguracionParametroCons.MONEDA.id).then((configuracion) => {

        this.monedaDefault = configuracion[0];
        console.log('moneda configuracion');
        console.dir(this.monedaDefault);
        this._prestamoService.getParametrosDetBDbyFilter(null, this.monedaDefault.id1).then((monedas) => {
          console.log('moneda por defecto');
          this.monedaDefault = monedas[0];
          console.dir(this.monedaDefault);
        });

      }, (error) => {
        console.log('Error al consultar la configuracion');
        console.dir(error);
      });

    }

  }

  nuevoPrestamo() {
    console.log('nuevo prestamo');
    this._navController.push(PrestamoNuevoComponent, { tipoOperacion: this.tipoOperacion });
  }

  clickItem(prestamo) {

    let args = [];
    if (!prestamo.isEstadoPagado()) {
      args.push({
        text: 'Marcar como pagado',
        icon: 'md-archive',
        disabled: 'true',
        handler: () => {
          if (this.platform.is('mobileweb')) {
            this._prestamoService.updatePrestadmoLS(prestamo.id, EstadoPrestamoParametroCons.PAGADO.id);
            this.showToast("El prestamo fué marcado como pagado");
            this.cargarPrestamos();
          } else if (this.platform.is('android')) {
            console.log('marcar como pagado');
            this._prestamoService.pagarPrestadmoBD(prestamo.id, EstadoPrestamoParametroCons.PAGADO.id).then((response) => {
              console.log('prestamo actualizado ');
              console.dir(response);
              this.showToast("El prestamo fué marcado como pagado");
              this.cargarPrestamos();
            }, (error) => {
              console.log('Error al marcar el prestamo como pagado');
              console.dir(error);
              this.showToast("Error al marcar el prestamo como pagado");
            });
          }
        }
      });
    }
    args.push(
      {
        text: 'Editar',
        icon: 'md-create',
        handler: () => {
          console.log('Editar clicked');
          this._navController.setRoot(PrestamoNuevoComponent, { prestamo: prestamo, contacto: prestamo.contacto });
        }
      }
    );
    args.push(
      {
        text: 'Eliminar',
        icon: 'md-trash',
        role: 'destructive',
        handler: () => {
          console.log('Play clicked');
          this.showConfirmDelete(prestamo);
        }
      }
    );

    args.push(
      {
        text: 'Cancelar',
        role: 'cancel', // will always sort to be on the bottom
        icon: !this.platform.is('ios') ? 'close' : null,
        handler: () => {
          console.log('Cancel clicked');
        }
      }
    );

    console.log('El prestamo seleccionado ' + prestamo.id);
    let actionSheet = this.actionsheetCtrl.create({
      title: 'Acciones',
      cssClass: 'action-sheets-basic-page',
      buttons: args
    });
    actionSheet.present();
  }

  showToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 1500,
      position: 'top'
    });
    toast.present();
  }

  showConfirmDelete(prestamo) {
    let confirm = this.alertCtrl.create({
      title: 'Eliminar Prestamo?',
      message: 'Seguro de eliminar?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('no clicked');
          }
        },
        {
          text: 'Si',
          handler: () => {
            console.log('si clicked');
            this._prestamoService.deletePrestadmoBD(prestamo.id).then((response) => {
              console.log('prestamo eliminado ');
              console.dir(response);
              this.showToast('Prestamo eliminado');
              this.cargarPrestamos();
            }, (error) => {
              console.log('Error al eliminar el prestamo');
              console.dir(error);
              this.showToast('Error al eliminar el prestamo');
            });

          }
        }
      ]
    });
    confirm.present();
  }


  isTipoOperacionPresto(): boolean {
    if (this.tipoOperacion == TipoOperacionParametroCons.PRESTO.id) {
      return true;
    }
    return false;
  }

  isTipoOperacionPresta(): boolean {
    if (this.tipoOperacion == TipoOperacionParametroCons.PRESTA.id) {
      return true;
    }
    return false;
  }

}
