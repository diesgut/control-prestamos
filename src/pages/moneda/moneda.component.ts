import { Component, OnInit } from '@angular/core';
import { Platform, ToastController } from 'ionic-angular';
import {PrestamoService} from '../../app/services/prestamo.service';
import { ConfiguracionParametroCons} from '../../app/models/constants/parametrosCons';
import {ParametroEnum} from '../../app/models/enums/parametroEnum';
import {ParametroDetalle} from '../../app/models/parametroDetalle';

@Component({
  selector: 'app-moneda',
  templateUrl: 'moneda.component.html'
  //, providers: [Contacts]
})
export class MonedaComponent implements OnInit {

  monedas: ParametroDetalle[] = [];

  monedaDefault: ParametroDetalle;

  ngOnInit() {

    this.monedaDefault = new ParametroDetalle(1, 1, "", "", "", "", 1);

  }

  constructor(private _prestamoService: PrestamoService,
    private platform: Platform,
    private toastCtrl: ToastController) {
    this.cargarMonedas();


  }


  cargarMonedas() {
    if (!this.platform.is('mobileweb')) {

      this._prestamoService.getParametrosDetBDbyFilter(ParametroEnum.MONEDA, null).then((monedas) => {
        this.monedas = monedas;
        console.dir(monedas);
        console.log('cantidad monedas ' + monedas.length);
      }, (error) => {
        console.log('Error al consultar las monedas');
        console.dir(error);
      });

      this._prestamoService.getParametrosDetBDbyFilter(ParametroEnum.CONFIGURACION, ConfiguracionParametroCons.MONEDA.id).then((configuracion) => {

        this.monedaDefault = configuracion[0];
        console.log('moneda configuracion');
        console.dir(this.monedaDefault);
        this._prestamoService.getParametrosDetBDbyFilter(null, this.monedaDefault.id1).then((monedas) => {
          console.log('moneda por defecto');
          this.monedaDefault = monedas[0];
          console.dir(this.monedaDefault);
        });

      }, (error) => {
        console.log('Error al consultar la configuracion');
        console.dir(error);
      });

    }

  }

  changeMoneda(even) {
    console.log('se cambio la moneda al id a ' + this.monedaDefault.id);

    this._prestamoService.getParametrosDetBDbyFilter(ParametroEnum.CONFIGURACION, ConfiguracionParametroCons.MONEDA.id).then((configuracion) => {
      let monedaConfigurada = configuracion[0];
      monedaConfigurada.id1 = this.monedaDefault.id;

      this._prestamoService.updateParameter(monedaConfigurada).then(data => {
        console.log('Moneda por defecto actualizada ' + data);
        this.showToast('Moneda por defecto actualizada');
      });


    }, (error) => {
      console.log('Error al consultar la moneda');
      console.dir(error);
      this.showToast('Error al cambiar la moneda por defecto');
    });

  }

  showToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 1500,
      position: 'top'
    });
    toast.present();
  }


}
