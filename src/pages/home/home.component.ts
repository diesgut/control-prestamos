import { Component, OnInit } from '@angular/core';
import { Platform } from 'ionic-angular';
import {PrestamoService} from '../../app/services/prestamo.service';
import {EstadoPrestamoParametroCons, TipoOperacionParametroCons, ConfiguracionParametroCons} from '../../app/models/constants/parametrosCons';
import {Resumen} from '../../app/models/interface/resumen';
import {ParametroDetalle} from '../../app/models/parametroDetalle';
import {ParametroEnum} from '../../app/models/enums/parametroEnum';

@Component({
  selector: 'app-home',
  templateUrl: 'home.component.html'
  //, providers: [Contacts]
})
export class HomeComponent implements OnInit {

  resumenPrestamos: { [key: number]: any; } = {};
  resumenDeudas: { [key: number]: any; } = {};
  today: string;

  estadoPrestamoParametroCons = EstadoPrestamoParametroCons;

  monedaDefault: ParametroDetalle;


  ngOnInit() {
    this.cargarResumen();


  }

  constructor(private _prestamoService: PrestamoService,
    private platform: Platform) {
    this.monedaDefault = new ParametroDetalle(1, 1, '', '', '', '', 1);

    let hoy = new Date();
    //    today.setDate(today.getDate() - 1);
    this.today = hoy.toISOString();
  }

  cargarResumen() {
    console.log('resumen');
    console.dir(this.platform);
    if (!this.platform.is('mobileweb')) {
      console.log('NO ES MOBILWEB');
      this._prestamoService.getPrestamosResumenBD(TipoOperacionParametroCons.PRESTO.id).then((resumenes) => {
        console.log('cantidad prestamos ' + resumenes.length);
        console.dir(resumenes);
        for (let resumen of resumenes) {
          let data: Resumen = { estado: 0, cantidad: resumen.cantidad, monto: resumen.monto };
          this.resumenPrestamos[resumen.estado] = data;
        }
      }, (error) => {
        console.log('Error al consultar los prestamos guardados');
        console.dir(error);
      });
      this._prestamoService.getPrestamosResumenBD(TipoOperacionParametroCons.PRESTA.id).then((resumenes) => {
        console.log('cantidad deudas ' + resumenes.length);
        console.dir(resumenes);
        for (let resumen of resumenes) {
          let data: Resumen = { estado: 0, cantidad: resumen.cantidad, monto: resumen.monto };
          this.resumenDeudas[resumen.estado] = data;
        }
      }, (error) => {
        console.log('Error al consultar los prestamos guardados');
        console.dir(error);
      });



      this._prestamoService.getParametrosDetBDbyFilter(ParametroEnum.CONFIGURACION, ConfiguracionParametroCons.MONEDA.id).then((configuracion) => {

        this.monedaDefault = configuracion[0];
        if (this.monedaDefault != null && this.monedaDefault != undefined) {
          console.log('moneda configuracion');
          console.dir(this.monedaDefault);
          this._prestamoService.getParametrosDetBDbyFilter(null, this.monedaDefault.id1).then((monedas) => {
            console.log('moneda por defecto');
            this.monedaDefault = monedas[0];
            console.dir(this.monedaDefault);
          });
        } else {
          this.monedaDefault = new ParametroDetalle(1, 1, "$", "$", "$", "$", 1);
        }
      }, (error) => {
        console.log('Error al consultar la configuracion');
        console.dir(error);
      });




    } else {
      console.log('ES MOBILWEB');
    }

    let data: Resumen = { estado: 0, cantidad: 0, monto: 0 };

    if (this.resumenDeudas[EstadoPrestamoParametroCons.PAGADO.id] == null || this.resumenDeudas[EstadoPrestamoParametroCons.PAGADO.id] == undefined) {
      this.resumenDeudas[EstadoPrestamoParametroCons.PAGADO.id] = data;
    }

    if (this.resumenDeudas[EstadoPrestamoParametroCons.PENDIENTE.id] == null || this.resumenDeudas[EstadoPrestamoParametroCons.PENDIENTE.id] == undefined) {
      this.resumenDeudas[EstadoPrestamoParametroCons.PENDIENTE.id] = data;
    }

    if (this.resumenDeudas[EstadoPrestamoParametroCons.VENCIDO.id] == null || this.resumenDeudas[EstadoPrestamoParametroCons.VENCIDO.id] == undefined) {
      this.resumenDeudas[EstadoPrestamoParametroCons.VENCIDO.id] = data;
    }

    if (this.resumenPrestamos[EstadoPrestamoParametroCons.PAGADO.id] == null || this.resumenPrestamos[EstadoPrestamoParametroCons.PAGADO.id] == undefined) {
      this.resumenPrestamos[EstadoPrestamoParametroCons.PAGADO.id] = data;
    }

    if (this.resumenPrestamos[EstadoPrestamoParametroCons.PENDIENTE.id] == null || this.resumenPrestamos[EstadoPrestamoParametroCons.PENDIENTE.id] == undefined) {
      this.resumenPrestamos[EstadoPrestamoParametroCons.PENDIENTE.id] = data;
    }

    if (this.resumenPrestamos[EstadoPrestamoParametroCons.VENCIDO.id] == null || this.resumenPrestamos[EstadoPrestamoParametroCons.VENCIDO.id] == undefined) {
      this.resumenPrestamos[EstadoPrestamoParametroCons.VENCIDO.id] = data;
    }

    console.log("Resumenes");
    console.dir(this.resumenDeudas);
    console.dir(this.resumenPrestamos);

  }


}
