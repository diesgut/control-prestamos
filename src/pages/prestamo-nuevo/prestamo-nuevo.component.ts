import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {NavController, NavParams, ToastController, Platform, AlertController} from 'ionic-angular';
import {MyApp} from '../../app/app.component';
import {PrestamosComponent} from '../../pages/prestamos/prestamos.component';
//import {FormGroup, FormBuilder, FormControl, Validators} from "@angular/forms";
//import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts';

import {Prestamo} from '../../app/models/prestamo';
import {Contacto} from '../../app/models/contacto';
import {ParametroEnum} from '../../app/models/enums/parametroEnum';
import {ParametroDetalle} from '../../app/models/parametroDetalle';
import {PrestamoService} from '../../app/services/prestamo.service';
import {MonedaParametrosCons, EstadoPrestamoParametroCons, ConfiguracionParametroCons} from '../../app/models/constants/parametrosCons';


@Component({
  selector: 'app-prestamo-nuevo',
  templateUrl: 'prestamo-nuevo.component.html'
  //, providers: [Contacts]
})
export class PrestamoNuevoComponent implements OnInit {

  public prestamo: Prestamo;
  public contacto: Contacto;
  public monedaDefault: ParametroDetalle;


  tiposOperaciones: ParametroDetalle[] = [];
  monedas: ParametroDetalle[] = [];
  contactlist: any;


  constructor(private _prestamoService: PrestamoService,
    private _navController: NavController,
    private toastCtrl: ToastController,
    private platform: Platform,
    private _navParams: NavParams,
    private alertCtrl: AlertController
    //  ,private contacts: Contacts
  ) {

    //   this.prestamo=new Prestamo("", "", 0, "", "", "", new Date(), new Date(), new Date(), 0, 0, "");
    this.prestamo = new Prestamo();
    this.contacto = new Contacto(null, null, null);

    let today: Date = new Date();

    this.prestamo.fechaRegistro = this._prestamoService.convertToDateStr(today);
    this.prestamo.fechaPrestamo = this._prestamoService.convertToDateStr(today);
    today.setDate(today.getDate() + 1);
    this.prestamo.fechaVencimiento = this._prestamoService.convertToDateStr(today);
    console.log("###############################################");
    console.log(this.prestamo.fechaRegistro);

    //  this.prestamo.tipoOperacion = TipoOperacionParametroCons.PRESTO.id;
    this.prestamo.moneda = MonedaParametrosCons.PEN.id;
    this.prestamo.estado = EstadoPrestamoParametroCons.PENDIENTE.id;

    if (_navParams.get("tipoOperacion") != null) {
      this.prestamo.tipoOperacion = _navParams.get("tipoOperacion");
    } else {
      this.prestamo = _navParams.get("prestamo");
      this.contacto = _navParams.get("contacto");
    }
    //console.dir(Contacts);

  }


  ngOnInit() {
    if (!this.platform.is('mobileweb')) {
      this.tiposOperaciones = this._prestamoService.getParametroDetalleByParent(ParametroEnum.TIPO_OPERACION);
      //  this.monedas = this._prestamoService.getParametroDetalleByParent(ParametroEnum.MONEDA);
      this._prestamoService.getParametrosDetBDbyFilter(ParametroEnum.MONEDA, null).then((monedas) => {
        this.monedas = monedas;
        console.dir(monedas);
        console.log('cantidad monedas ' + monedas.length);
      }, (error) => {
        console.log('Error al consultar las monedas');
        console.dir(error);
      });

      if (this.prestamo.id == null) {
        this._prestamoService.getParametrosDetBDbyFilter(ParametroEnum.CONFIGURACION, ConfiguracionParametroCons.MONEDA.id).then((configuracion) => {
          console.log('configuracion!!!');
          this.monedaDefault = configuracion[0];

          this._prestamoService.getParametrosDetBDbyFilter(null, this.monedaDefault.id1).then((monedas) => {
            console.log('moneda por defecto');
            this.monedaDefault = monedas[0];
            this.prestamo.moneda = this.monedaDefault.id;
            console.dir(this.monedaDefault);
          });

        }, (error) => {
          console.log('Error al consultar la configuracion');
          console.dir(error);
        });
      }
    }
  }

  guardarPrestamo(form: NgForm) {

    console.log('guardar prestamo');
    console.dir(form);
    console.dir(this.prestamo);
    console.dir(this.contacto);

    if (!form.valid) {
      this.showAlert('Alerta', 'Datos Incompletos', 'danger');
      return;
    }

    //  let message: string = "";
    if (this.prestamo.id == null) {
      if (this.platform.is('mobileweb')) {
        console.log('guardar mobileweb');
        this._prestamoService.guardarPrestamoLS(this.prestamo, this.contacto);

        this.showToast('Prestamo guardado');
        this._navController.push(MyApp);

      } else if (this.platform.is('android')) {
        console.log('guardar android');
        this._prestamoService.guardarPrestamoBD(this.prestamo, this.contacto).then(data => {
          console.log('Prestamo guardado ' + data);
          this.showToast('Prestamo guardado');
          //  this._navController.push(MyApp);
          this._navController.setRoot(PrestamosComponent, { tipoOperacion: this.prestamo.tipoOperacion });
        });
      }
    } else {

      console.log('actualizar android');
      this._prestamoService.updatePrestamoBD(this.prestamo, this.contacto).then(data => {
        console.log('Prestamo actualizado ' + data);
        this.showToast('Prestamo actualizado');
        //  this._navController.push(MyApp);
        this._navController.setRoot(PrestamosComponent, { tipoOperacion: this.prestamo.tipoOperacion });
      });

    }
  }

  cancelar() {
    //  this._navController.push(MyApp);
    //  console.dir(this.nav);
    this._navController.setRoot(PrestamosComponent, { tipoOperacion: this.prestamo.tipoOperacion });
  }

  showToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 1500,
      position: 'top'
    });
    toast.present();
  }

  showAlert(title: string, message: string, classCss: string) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['OK'],
      cssClass: classCss
    });
    alert.present();
  }

}
